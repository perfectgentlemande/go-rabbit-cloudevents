module bitbucket.org/perfectgentlemande/go-rabbit-cloudevents

go 1.15

require (
	github.com/cloudevents/sdk-go/protocol/amqp/v2 v2.4.1
	github.com/cloudevents/sdk-go/v2 v2.4.1
	github.com/google/uuid v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/streadway/amqp v1.0.0
)
