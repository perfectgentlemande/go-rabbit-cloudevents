package main

import (
	"context"
	"encoding/json"
	"github.com/cloudevents/sdk-go/v2/binding"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"time"

	ceamqp "github.com/cloudevents/sdk-go/protocol/amqp/v2"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	swamqp "github.com/streadway/amqp"
	"log"
	"strings"
)

func sampleConfig() (server, node string, opts []ceamqp.Option) {
	addr := "amqp://localhost:55005"

	return addr, strings.TrimPrefix(addr, "/"), opts
}

// Example is a basic data struct.
type Example struct {
	Sequence int    `json:"id"`
	Message  string `json:"message"`
}

const (
	count = 10
)

type rabbitCli struct {
	amqpChannel *swamqp.Channel
	conn *swamqp.Connection
}

func (rc *rabbitCli) Send(ctx context.Context, m binding.Message, transformers ...binding.Transformer) error {
	event := cloudevents.NewEvent()
	event.SetID(uuid.New().String())
	event.SetSource("https://github.com/cloudevents/sdk-go/v2/samples/sender")
	event.SetTime(time.Now())
	event.SetType("com.cloudevents.sample.sent")

	data, err := json.Marshal(event)
	if err != nil {
		return errors.Wrap(err, "cannot marshal event")
	}

	return rc.amqpChannel.Publish("", "add", false, false, swamqp.Publishing{
		Headers:         nil,
		ContentType:     "application/json",
		ContentEncoding: "",
		DeliveryMode:    swamqp.Persistent,
		Priority:        0,
		CorrelationId:   "",
		ReplyTo:         "",
		Expiration:      "",
		MessageId:       "",
		Timestamp:       time.Time{},
		Type:            "",
		UserId:          "",
		AppId:           "",
		Body:            data,
	})
}

func NewRabbitCli(addr string) (*rabbitCli, error) {
	var err error
	cli := &rabbitCli{}

	cli.conn, err = swamqp.Dial(addr)
	if err != nil {
		return nil, errors.Wrap(err, "cannot create connection")
	}

	cli.amqpChannel, err = cli.conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "cannot create amqp channel")
	}

	return cli, nil
}
func (rc *rabbitCli) Close() error {
	if err := rc.amqpChannel.Close(); err != nil {
		return errors.Wrap(err, "cannot close channel")
	}
	if err := rc.conn.Close(); err != nil {
		return errors.Wrap(err, "cannot close connection")
	}

	return nil
}

func main() {
	rabCli, err := NewRabbitCli("amqp://localhost:55005")
	if err != nil {
		log.Fatal(err)
	}
	defer rabCli.Close()

	//host, node, opts := sampleConfig()
	//p, err := ceamqp.NewProtocol(host, node, []amqp.ConnOption{}, []amqp.SessionOption{}, opts...)
	//if err != nil {
	//	log.Fatalf("Failed to create amqp protocol: %v", err)
	//}
	//
	//// Close the connection when finished
	//defer p.Close(context.Background())

	// Create a new client from the given protocol
	c, err := cloudevents.NewClient(rabCli)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	for i := 0; i < count; i++ {
		event := cloudevents.NewEvent()
		event.SetID("shit")
		event.SetSource("https://github.com/cloudevents/sdk-go/v2/samples/sender")
		event.SetTime(time.Now())
		event.SetType("com.cloudevents.sample.sent")

		err := event.SetData(cloudevents.ApplicationJSON,
			&Example{
				Sequence: i,
				Message:  "Hello world!",
			})
		if err != nil {
			log.Fatalf("Failed to set data: %v", err)
		}

		if result := c.Send(context.Background(), event); cloudevents.IsUndelivered(result) {
			log.Fatalf("Failed to send: %v", result)
		} else if cloudevents.IsNACK(result) {
			log.Printf("Event not accepted: %v", result)
		}
		time.Sleep(100 * time.Millisecond)
	}
}